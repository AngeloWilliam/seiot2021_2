#ifndef __USER_CONSOLE__
#define __USER_CONSOLE__
#include "Arduino.h" 

#define NUM_INPUT_POS 4
#define LAST_POS 3

#define NO_SELECTION 255
#define MULTIPLE_SELECTIONS 254

void initPlayerConsole();

void printOnTheConsole(const String& msg);

/* read the difficulty level */
uint8_t readDifficultyLevel();

/* check if the player started the game */
bool playerInputStarted();

/* reset the input, at each interation */
void resetPlayerInput();

/*
 * wait for player input with timeout
 * return values:
 * 0..NUM INPUT - SINGLE INPUT SELECTED
 * 255 NO INPUT SELECTED
 * 254 MULTIPLE INPUTS SELECTED
 */
uint8_t waitForSelectedInputWithTimeout(long timeOut);

/* for testing */
void testPlayerInput();


#endif
