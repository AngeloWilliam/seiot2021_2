#include "user_console.h"
#include "Arduino.h"
#include "config.h"

#include <EnableInterrupt.h>

#define BOUNCING_TIME 100

uint8_t inputPins[] = {BUT01_PIN, BUT02_PIN, BUT03_PIN, BUT04_PIN};

bool buttonPressedFlags[] = {false, false, false, false};

long lastButtonPressedTimeStamps[NUM_INPUT_POS];

void buttonHandler(){
  long ts = millis();
  for (int i = 0; i < NUM_INPUT_POS; i++) {
    int status = digitalRead(inputPins[i]);
    if (status == HIGH) {
      if (ts - lastButtonPressedTimeStamps[i] > BOUNCING_TIME){
        buttonPressedFlags[i] = true;      
        lastButtonPressedTimeStamps[i] = ts;
      }
    } else {
        buttonPressedFlags[i] = false;      
        lastButtonPressedTimeStamps[i] = ts;
    }
  }
}

uint8_t readDifficultyLevel(){
  int value = ((uint8_t)(analogRead(POT_PIN) / 128)) + 1;
  return value;
}

void testPlayerInput(){
  for (int i = 0; i < NUM_INPUT_POS; i++) {
    if (buttonPressedFlags[i]) {
      Serial.println(String("button ") + i + " pressed"); 
    }
  }
  int value = analogRead(POT_PIN);
  Serial.println(value);
}

void initPlayerConsole(){
  Serial.begin(9600);
  for (int i = 0; i < NUM_INPUT_POS; i++) {
    pinMode(inputPins[i], INPUT);  
    enableInterrupt(inputPins[i], buttonHandler, CHANGE);       
  }
}

void printOnTheConsole(const String& msg){
  Serial.println(msg);
}


void resetPlayerInput(){
  long ts = millis();
  for (int i = 0; i < NUM_INPUT_POS; i++) {
    buttonPressedFlags[i] = false;      
    lastButtonPressedTimeStamps[i] = ts;    
  }
  delay(BOUNCING_TIME);
}

uint8_t waitForSelectedInputWithTimeout(long timeOut) {
  bool found = false;
  uint8_t pos = 255;
  long t = millis();
  long tmax = t + timeOut;
  while (t < tmax) {  
    for (int i = 0; i < NUM_INPUT_POS; i++) {
      if (buttonPressedFlags[i]) {
         if (found){
           return MULTIPLE_SELECTIONS;              
         } else {
            found = true;
            pos = i;
         }
      }
    }  
    if (found){
      return pos;
    } else {
      t = millis();
    }
  }
  return NO_SELECTION;
}

bool playerInputStarted(){
  if (buttonPressedFlags[0]){
    return true;
  } else {
    return false;
  }
}
