/*
 * SEIOT a.a. 2020/2021
 *
 * PROGETTO #1 - TRACK THE LED FLY 
 * 
 * Esempio di soluzione.
 */
#include "config.h"
#include "game_core.h"
#include "user_console.h" 
#include "led_board.h"

void setup() {
  initPlayerConsole();
  initLedBoard();
  gameState = GAME_INTRO;
}

void loop(){ 
  switch (gameState) { 
  case GAME_INTRO:
    gameIntro();
    break;
  case GAME_WAIT_TO_START:
    gameWaitToStart();
    break;
  case GAME_INIT:
    gameInit();
    break;
  case GAME_LOOP:
    gameLoop();
    break;
  case GAME_OVER:
    gameOver();
    break;
  }
}
