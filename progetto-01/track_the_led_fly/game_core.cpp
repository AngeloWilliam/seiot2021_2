#include "game_core.h"
#include "Arduino.h"
#include "config.h"
#include "led_board.h"
#include "user_console.h"

#define K 10

/* current position of the fly */
uint8_t fly_position;

/* selected difficulty level */
uint8_t difficultyLevel = 1;

/* current minimum waiting time */
long tmin;

/* the score */
long score = 0;

/* current game state */
uint8_t gameState;

/* =========== procedures about the fly ======== */

void reset_fly_pos(uint8_t newPos){
  fly_position = newPos;
  switchOnLed(fly_position);
}

void move_fly_to(uint8_t newPos){
  switchOffLed(fly_position);
  fly_position = newPos;
  switchOnLed(fly_position);
}

void move_fly_to_random_adjacent_pos(){
  uint8_t delta = random(0,2);
  int newPos = fly_position;
  if (delta == 0){
    if (newPos > 0) {
      newPos--;
    } else {
      newPos = LAST_POS; 
    }
  } else {
    if (newPos < LAST_POS) {
      newPos++;
    } else {
      newPos = 0; 
    }
  }
  move_fly_to(newPos);
}

void test_fly_moving(){
  reset_fly_pos(0);
  while (true) {
     uint8_t newPos = fly_position;
     while (newPos == fly_position){
      newPos = (uint8_t) random(0, NUM_INPUT_POS);
     }
     move_fly_to(newPos);
     delay(200); 
  }    
}

/* =========== procedures about game state ======== */


void gameIntro(){
  resetLedBoard();   
  printOnTheConsole("Welcome to the Track to Led Fly Game. Press Key T1 to Start");
  resetPulsing();
  gameState = GAME_WAIT_TO_START;  
};

void gameWaitToStart(){
  goOnPulsing();
  if (playerInputStarted()){
    gameState = GAME_INIT;  
  } 
}

void gameInit(){
  resetPulsing();
  printOnTheConsole("Go!");  
  difficultyLevel = readDifficultyLevel();
  tmin = TMIN/difficultyLevel;
  score = 0;
  gameState = GAME_LOOP;  
};

void gameLoop(){
  resetPlayerInput();   
  move_fly_to_random_adjacent_pos();
  long waitTime = random(tmin,K*tmin);
  uint8_t selectedInput = waitForSelectedInputWithTimeout(waitTime);
  #ifdef __DEBUG__
  printOnTheConsole(String("selected input: ") + selectedInput + " - fly_position " + fly_position);
  #endif
  if (selectedInput == fly_position){
      printOnTheConsole(String("Tracking the fly: pos ") + fly_position);
      tmin = tmin*7/8;  
      delay(10);
      score++;  
  } else {
       gameState = GAME_OVER;  
  }
};

void gameOver(){
  printOnTheConsole(String("Game Over - Score: ") + score);  
  gameState = GAME_INTRO;
}

void gameTest(){
  testLedBoard();
  testPlayerInput();
  test_fly_moving();  
}
