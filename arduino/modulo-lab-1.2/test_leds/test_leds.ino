#define LED0 3
#define LED1 4
#define LED2 5

unsigned char leds[3]; 

void setup() {  
  leds[0] = LED0;
  leds[1] = LED1;
  leds[2] = LED2;

  for (int i = 0; i < 3; i++){
    pinMode(leds[i], OUTPUT);  
  }
}

void loop() {

  for (int i = 1; i < 3; i++){
    digitalWrite(leds[i], HIGH);
    delay(200);
    digitalWrite(leds[i], LOW);
  }

  for (int i = 1; i >= 0; i--){
    digitalWrite(leds[i], HIGH);
    delay(200);
    digitalWrite(leds[i], LOW);
  }
  // put your main code here, to run repeatedly:

}
