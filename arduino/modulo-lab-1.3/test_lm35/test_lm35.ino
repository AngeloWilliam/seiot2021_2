
#define temperaturePin A0
#define VCC ((float)5)

void setup() {
  Serial.begin(9600);
}

void loop() {
  int value = analogRead(temperaturePin);
  Serial.println(String("valore letto: ") + value);
  float valueInVolt = value*VCC/1023;  
  Serial.println(String("valore letto in volt: ") + valueInVolt);
  float valueInCelsius = valueInVolt/0.01;
  Serial.println(String("valore letto in gradi Celsius: ") + valueInCelsius);  
  delay(1000);
}
