#ifndef __EXPMAN_TASK__
#define __EXPMAN_TASK__

#include "Task.h"
#include "UserConsole.h"
#include "Experiment.h"
#include "BlinkingTask.h"

class ExpManTask: public Task {

public:
  ExpManTask(Experiment* pExperiment, UserConsole* pUserConsole, BlinkingTask* pBlinkTask);  
  void tick();

private:

  enum { IDLE, READY, ONGOING, ERROR, COMPLETED } state;
 
  Experiment* pExperiment;
  UserConsole* pUserConsole;
  BlinkingTask* pBlinkingTask;

  long errorTime;
  long sleepTime;
};

#endif
