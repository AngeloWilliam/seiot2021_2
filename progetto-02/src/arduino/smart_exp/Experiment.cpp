#include "Experiment.h"
#include "Logger.h"
#include "config.h"
#include "Arduino.h"


Experiment::Experiment() {
  time = -1;
  startTime = -1;
  state = NOT_STARTED;
};

void Experiment::init() {
  time = startTime = millis();
  pos = -1;
  vel = -1;
  acc = -1;
  state = ONGOING;
  objLost = false;
}

void Experiment::setTrackingFreq(TTrackFreq freq){
  trackFreq = freq;
}

TTrackFreq Experiment::getTrackingFreq(){
  return trackFreq;
}

void Experiment::updateData(double newPos){
  long newTime = millis();
  double dt = newTime - time;
  if (dt > 0) {
    time = newTime;
    if (pos != -1){
      double newVel = (newPos - pos)/dt;
      if (vel != -1){
        acc = (newVel - vel)/dt;
      } 
      vel = newVel;  
    }
    pos = newPos;
    // Logger.log(String("Experiment | ") + dt + " " + pos + " " + vel*1000  + " " + acc*1000);
  }
}

long Experiment::getLastTime(){
  return time - startTime;
}

float Experiment::getLastPos(){
  return pos;
}

float Experiment::getLastVel(){
  return vel;
}

float Experiment::getLastAcc(){
  return acc;
}

bool Experiment::started() {
  return state == ONGOING;
}

bool Experiment::completed() {
  return state == COMPLETED;
}

bool Experiment::interrupted() {
  return state == INTERRUPTED;
}

void Experiment::notifyObjLost(){
  objLost = true;
}

bool Experiment::isObjLost(){
  return objLost;
} 

void Experiment::setInterrupted(){
  state = INTERRUPTED;
}

void Experiment::setCompleted(){
  state = COMPLETED;
}
