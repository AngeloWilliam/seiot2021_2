#ifndef __TRACKSPEEDTASK__
#define __TRACKSPEEDTASK__

#include "Task.h"
#include "Tachimeter.h"
#include "Experiment.h"

class TrackSpeedTask: public Task {

public:
  TrackSpeedTask(Experiment* pExp); 
  void tick();

private:
  enum { IDLE, WORKING } state;
  Experiment* pExperiment;
  Tachimeter* pTachimeter;
  long timeForPositioning;
};

#endif
