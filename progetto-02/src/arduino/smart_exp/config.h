#ifndef __CONFIG__
#define __CONFIG__

#define SONAR_TRIG_PIN 7
#define SONAR_ECHO_PIN 8
#define TEMP_PIN A1

#define TACHI_SERVO_PIN 9

#define POT_PIN A0
#define BT_START_PIN 6
#define BT_STOP_PIN 5

#define PIR_PIN 2

#define L1_PIN 4
#define L2_PIN 3


#define MAX_TIME 50000
#define ERROR_TIME 5000
#define SLEEP_TIME 5000

#endif
