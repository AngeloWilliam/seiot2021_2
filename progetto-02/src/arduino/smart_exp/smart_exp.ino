/*
 * SMART EXP - Progetto 02 - SEIOT a.a. 2020-2021
 * 
 * Esempio di soluzione.
 * 
 */
#include "config.h"
#include "Scheduler.h"
#include "Logger.h"
#include "MsgService.h"
#include "ExpManTask.h"
#include "TrackingTask.h"
#include "TrackSpeedTask.h"
#include "UserConsole.h"

Scheduler sched;

void setup(){
  Logger.log(".:: Smart Experiment ::.");

  sched.init(10);
  MsgService.init();

  UserConsole* pUserConsole = new UserConsole();
  Experiment* pExperiment = new Experiment();  

  Task* pBlinkingTask = new BlinkingTask(pUserConsole->getLed2());
  pBlinkingTask->init(40);
  pBlinkingTask->setActive(false);
  
  Task* pExpManTask = new ExpManTask(pExperiment, pUserConsole, pBlinkingTask);
  pExpManTask->init(40);

  Task* pTrackingTask = new TrackingTask(pExperiment, pUserConsole);
  pTrackingTask->init();

  Task* pTrackSpeedTask = new TrackSpeedTask(pExperiment);
  pTrackSpeedTask->init(100);
 
  sched.addTask(pExpManTask); 
  sched.addTask(pTrackingTask); 
  sched.addTask(pTrackSpeedTask); 
  sched.addTask(pBlinkingTask); 

  Logger.log("Calibrating...");
  pUserConsole->notifyCalibrating();
  pUserConsole->calibrate();
  Logger.log("done");
  pUserConsole->notifyReady();
}

void loop(){
  sched.schedule();
}
