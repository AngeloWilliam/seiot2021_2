#include "Tachimeter.h"
#include "Logger.h"
#include "config.h"
#include "servo_motor_impl.h"

Tachimeter::Tachimeter(double maxSpeed) : maxSpeed(maxSpeed) {
    pServo = new ServoMotorImpl(TACHI_SERVO_PIN);
}

void Tachimeter::enable(){
    pServo->on();
    pServo->setPosition(0);
}

void Tachimeter::disable(){
    pServo->off();
}

void Tachimeter::setSpeed(double speed){
    int pos = (int)(speed*180/maxSpeed);
    Logger.log(String("motor ") + pos);
    if (pos > 180) {
      pos = 180;
    } else if (pos < 0) {
      pos = 0;
    }
    pServo->setPosition(pos);
}
